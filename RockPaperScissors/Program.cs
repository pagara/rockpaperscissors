﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RockPaperScissors
{
    class Program
    {

        private static Rules _rules = new Rules();
        public static void Main(string[] args)
        {
            Console.WriteLine("Available moves:");
            Console.WriteLine("Rock, Scissors or Paper");
            
            //ConsoleKey.D1;
            while (true)
            {
                Console.WriteLine();
                Console.WriteLine("Player 1:");
                var playerOneMove = Console.ReadLine();

                Console.WriteLine("Player 2:");
                var playerTwoMove = Console.ReadLine();

                try
                {
                    var winner = _rules.SubmitMoves(playerOneMove, playerTwoMove);
                    Console.WriteLine(winner + " wins!");
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    continue;
                }
                Console.WriteLine();
            }
        }
    }
}
