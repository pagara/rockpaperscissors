﻿using RockPaperScissors.enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RockPaperScissors
{
    public class Rules
    {
        public Moves SubmitMoves(string playerOneMove, string playerTwoMove)
        {
            var playerOne = TranslateMoves(playerOneMove);
            var playerTwo = TranslateMoves(playerTwoMove);
            return EvaluateMoves(playerOne, playerTwo);
        }

        public Moves EvaluateMoves(Moves playerOneMove, Moves playerTwoMove)
        {

            if(playerOneMove == Moves.Paper && playerTwoMove == Moves.Rock
            || playerOneMove == Moves.Rock && playerTwoMove == Moves.Paper)
            {
                return Moves.Paper;
            }
            else if (playerOneMove == Moves.Paper && playerTwoMove == Moves.Scissors
                  || playerOneMove == Moves.Scissors && playerTwoMove == Moves.Paper)
            {
                return Moves.Scissors;
            }
            else if (playerOneMove == Moves.Rock && playerTwoMove == Moves.Scissors
                  || playerOneMove == Moves.Scissors && playerTwoMove == Moves.Rock)
            {
                return Moves.Rock;
            }
            throw new Exception("Draw! Try again!");
        }

        public Moves TranslateMoves(string move)
        {
            try
            {
                move = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(move.ToLower());
                return (Moves)Enum.Parse(typeof(Moves), move);
            }
            catch(Exception)
            {
                throw new Exception("Move not recognised! Try again");
            }
            
        }
    }
}
